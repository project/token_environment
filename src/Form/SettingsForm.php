<?php

namespace Drupal\token_environment\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for token_environment.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'token_environment_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['token_environment.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('token_environment.settings');

    $form['allowed_env_variables'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Allowed environment variables'),
      '#required' => FALSE,
      '#description' => $this->t("One variable name per line (without a leading $). Tokens will be available as <em>env:[ENV_VAR_NAME]</em> (tokens are case sensitive)"),
    ];

    $allowedEnvVariables = $config->get('allowed_env_variables');
    if (is_array($allowedEnvVariables)) {
      $form['allowed_env_variables']['#default_value'] = implode("\n", $allowedEnvVariables);
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('token_environment.settings');
    $allowedEnvVariables = explode("\n", $form_state->getValue('allowed_env_variables'));
    $allowedEnvVariables = array_map('trim', $allowedEnvVariables);
    // Remove empty values.
    $allowedEnvVariables = array_filter($allowedEnvVariables);
    $config
      ->set('allowed_env_variables', $allowedEnvVariables)
      ->save();

    parent::submitForm($form, $form_state);
  }

}
