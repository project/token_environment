<?php

/**
 * @file
 * Token integration for the Tokens from environment module.
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function token_environment_token_info() {
  $config = \Drupal::config("token_environment.settings");
  $allowedEnvVariables = $config->get("allowed_env_variables");

  $info = [];
  if (!empty($allowedEnvVariables)) {
    $info['types']['env'] = [
      'name' => t('Environment'),
      'description' => t('Environment tokens.'),
    ];

    foreach ($allowedEnvVariables as $allowedEnvVariable) {
      $info['tokens']['env'][$allowedEnvVariable] = [
        'name' => $allowedEnvVariable,
        'description' => t("The value of the environment variable '@var_name'", ["@var_name" => $allowedEnvVariable]),
      ];
    }
  }
  return $info;
}

/**
 * Implements hook_tokens().
 */
function token_environment_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];
  if ($type == 'env') {
    $config = \Drupal::config("token_environment.settings");
    $allowedEnvVariables = $config->get("allowed_env_variables");
    if (!empty($allowedEnvVariables)) {
      foreach ($tokens as $name => $original) {
        if (in_array($name, $allowedEnvVariables)) {
          $replacements[$original] = getenv($name);
        }
      }
    }
  }
  return $replacements;
}
