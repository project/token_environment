# Tokens from environment

Provides tokens for environment variables.

It allows to define environment variables that will be available as tokens (as <em>env:[ENV_VAR_NAME]</em>). Those
tokens will then be replaced by the [Token Service](https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Utility!Token.php/class/Token/8.8.x).

Any environment variable accessible through PHP's `getenv()` can be defined as a token.
 
## Drupal 8 & 9 ##
 This module is compatible with Drupal 8 and Drupal 9.
 
## Drupal 7 ##
There is a Drupal 7 compatible module called [token_env](https://www.drupal.org/project/token_env).
