<?php

namespace Drupal\Tests\token_environment\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tokens from environment test class.
 */
class TokenEnvironmentTest extends KernelTestBase {
  public static $modules = ['system', 'token_environment'];

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
    $this->config('token_environment.settings')->set('allowed_env_variables', ['ENV_VARIABLE_1', 'ENV_VARIABLE_2'])->save();
  }

  /**
   * Tests tokens are replaced.
   */
  public function testTokenFromEnvironment() {
    putenv("ENV_VARIABLE_1=VALUE_1");
    putenv("ENV_VARIABLE_2=VALUE_2");
    $token_service = \Drupal::service('token');
    $this->assertEquals("VALUE_1", $token_service->replace("[env:ENV_VARIABLE_1]"));
    $this->assertEquals("VALUE_2", $token_service->replace("[env:ENV_VARIABLE_2]"));
    $this->assertEquals("[env:ENV_VARIABLE_NOT_ALLOWED]", $token_service->replace("[env:ENV_VARIABLE_NOT_ALLOWED]"));
  }

}
